defmodule Bob do
  def hey(str) do
    cond do
      String.trim(str) == "" -> "Fine. Be that way!"
      question(str) and shouting(str) == true ->
        "Calm down, I know what I'm doing!"
      question(str) == true ->
        "Sure."
      shouting(str) ->
         "Whoa, chill out!"
      true ->
         "Whatever."
    end
  end

  def question(str) do
    String.ends_with?(String.trim(str), "?")
  end

  def shouting(str) do
    String.upcase(str) == str and String.downcase(str) != str
  end
end

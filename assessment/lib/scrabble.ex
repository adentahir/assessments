defmodule Scrabble do
  @doc """
  Calculate the scrabble score for the word.
  """
  @spec score(String.t()) :: non_neg_integer
  def score(word) do
    _score((String.split(String.upcase(word), "", trim: true)))
  end

 def _score [] do
    0
  end

  def _score([head | tail ]) do
      alphabet_score =
      cond do
       _point(head) == 1 -> 1
       _point(head) == 2 -> 2
       _point(head) == 3 -> 3
       _point(head) == 4 -> 4
       _point(head) == 5 -> 5
       _point(head) == 8 -> 8
       _point(head) == 10 -> 10
       _point(head) == 0 -> 0
        true -> 0
      end
    alphabet_score + _score(tail)
  end

  def _point(chr) when chr in ["A", "E", "I", "O", "U", "L", "N", "R", "S", "T"] do
    1
  end

  def _point(chr) when chr in ["D", "G"] do
     2
  end

  def _point(chr) when chr in ["B", "C", "M", "P"] do
     3
  end

  def _point(chr) when chr in ["F", "H", "V", "W", "Y"] do
     4
  end

  def _point(chr) when chr == "K" do
      5
  end

  def _point(chr) when chr in ["J", "X"] do
     8
  end

  def _point(chr) when chr in ["Q", "Z"] do
    10
  end


  def _point(chr) do
    if String.trim(chr) == "" do
      0
    end
  end

end

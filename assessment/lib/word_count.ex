defmodule WordCount do




    #|> Enum.frequencies()
  #  |> Enum.map(fn %{x, y} -> %{x => 1} end)
  #  |> Enum.map(fn(x) -> %{x => Enum.count(x)} end)
  #  |> Enum.each(Enum.count(x) -> Enum.member?()

  def count(phrase) do

    phrase
    |> String.downcase()
    |> to_words()
    |> Enum.frequencies()
  end

  defp to_words(phrase) do

    ~r/[\s_,!&@$%^:.]+/
    |> Regex.split(phrase)
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(&normalize_word/1)
  end

  defp normalize_word(word) do
    word |> String.trim_leading("'") |> String.trim_trailing("'")

  end

end

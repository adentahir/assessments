defmodule Raindrops do


  @spec convert(pos_integer) :: String.t()

  def convert(number) do
    [
      get_factor_string(number, 3),
      get_factor_string(number, 5),
      get_factor_string(number, 7)
    ]
    |> Enum.join()
    |> case do
      "" -> Integer.to_string(number)
      string -> string
    end
  end

  @strings %{
    3 => "Pling",

    5 => "Plang",

    7 => "Plong"
  }


  def get_factor_string(number, prime_factor) do
    case rem(number, prime_factor) do
      0 -> Map.get(@strings, prime_factor)
      _ -> ""
    end
  end
end

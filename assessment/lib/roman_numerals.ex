defmodule RomanNumerals do
  def numeral(num) do

    cond do
      num < 10 && num >= 1 ->
        resolveUnit(num)
      num < 100 ->
         resolveTens(num) <> resolveUnit(num)
      num < 1000 ->
         resolveHundreds(num) <> resolveTens(num) <> resolveUnit(num)
      num < 3000 ->
        resolveThousands(num) <> resolveHundreds(num) <> resolveTens(num) <> resolveUnit(num)
      num == 3000 -> "MMM"
        true -> "out of range"
    end

  end

  def resolveUnit(num) do
    num =
      cond do
        num >= 1000 -> Kernel.rem(num, 10)
        num >= 100 -> Kernel.rem(num, 10)
        num >= 10 -> Kernel.rem(num, 10)
        true -> num
      end
      IO.inspect(num)
      cond do
        num == 1 -> "I"
        num == 2 -> "II"
        num == 3 -> "III"
        num == 4 -> "IV"
        num == 5 -> "V"
        num == 6 -> "VI"
        num == 7 -> "VII"
        num == 8 -> "VIII"
        num == 9 -> "IX"
        true -> ""
      end
  end

  def resolveTens(num) do
    num =
    cond do
      num >= 1000 -> Kernel.rem(num, 1000)
      num >= 100 -> Kernel.rem(num, 100)
      true -> num
    end

     cond do
       num < 10 -> ""
       num < 20 -> "X"
       num < 30 -> "XX"
       num < 40 -> "XXX"
       num < 50 -> "XL"
       num < 60 -> "L"
       num < 70 -> "LX"
       num < 80 -> "LXX"
       num < 90 -> "LXXX"
       num < 100 -> "XC"
     end
  end

  def resolveHundreds(num) do
    num =
      cond do
        num >= 1000 -> Kernel.rem(num, 1000)
        true -> num
      end

    cond do
      num < 100 -> ""
      num < 200 -> "C"
      num < 300 -> "CC"
      num < 400 -> "CCC"
      num < 500 -> "CD"
      num < 600 -> "D"
      num < 700 -> "DC"
      num < 800 -> "DCC"
      num < 900 -> "DCCC"
      num < 1000 -> "CM"
  end
 end

  @spec resolveThousands(any) :: nonempty_binary
  def resolveThousands(num) do

    cond do
      num < 2000 -> "M"
      num < 3000 -> "MM"
    end
  end

end

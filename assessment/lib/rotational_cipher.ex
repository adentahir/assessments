defmodule RotationalCipher do

  @spec rotate(text :: String.t(), shift :: integer) :: String.t()


  def rotate(text, shift) do
    text
    |> to_charlist
    |> Enum.map(&rotate_char(&1, shift))
    |> to_string
  end

  defp rotate_char(char, shift) when char in ?a..?z do
    rotate_with_offset(char, shift, ?a)
  end

  defp rotate_char(char, shift) when char in ?A..?Z do
    rotate_with_offset(char, shift, ?A)

  end

  defp rotate_char(char, _), do: char

  defp rotate_with_offset(char, shift, offset) do
    (char - offset + shift)
    |> Integer.mod(26)
    |> Kernel.+(offset)
  end
end

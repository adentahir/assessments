defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t(), [String.t()]) :: [String.t()]
  def match(base, candidates) do
      #base = String.downcase(base)
      #Enum.map(candidates, fn x -> String.downcase(x) end)
      for word <- candidates, anagram?(base, word), do: word
  end

  defp word_to_list(word) do
    word
    |> String.downcase()
    |> String.graphemes()
    |> Enum.sort()
  end

  defp anagram?(word_a, word_b) do
    cond do
      String.downcase(word_a) == String.downcase(word_b) -> false
      true -> word_to_list(word_a) == word_to_list(word_b)
    end
  end

end

defmodule RnaTranscription do

  def swap '' do
    ''
  end

  def swap([head | tail ]) do
  h =
  case head do
     ?G -> 'C'
     ?C -> 'G'
     ?T -> 'A'
     ?A -> 'U'
     x when x >= ?B and x <= ?z -> [x]
      _ -> ''
      end
    h ++ swap(tail)
  end

  def to_rna(dna) do
    swap(dna)
  end

end

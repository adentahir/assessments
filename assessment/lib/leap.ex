defmodule Year do

  def leap_year?(year) do
    if _div4(year) and _div100(year) and _div400(year) do
      true
    else
      false
    end
  end

  def _div4(x) do
    rem(x, 4) == 0
  end

  def _div100(x) do
    rem(x, 100) == 0
  end

  def _div400(x) do
    rem(x, 400) == 0
  end
end

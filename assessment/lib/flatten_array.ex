defmodule FlattenArray do

  def flatten(list) do
    do_flatten(list)
  end

  def do_flatten([]), do: []
  def do_flatten(nil), do: []
  def do_flatten([h | t]), do: flatten(h) ++ flatten(t)
  def do_flatten(h), do: [h]
  

end

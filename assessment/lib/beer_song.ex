defmodule BeerSong do
  @spec verse(integer) :: String.t()
  def verse(number) do
    cond do
      number > 2 ->
        "#{number} bottles of beer on the wall, #{number} bottles of beer.\n" <>
        "Take one down and pass it around, #{number - 1} bottles of beer on the wall.\n"
      number == 2 ->
        "2 bottles of beer on the wall, 2 bottles of beer.\n" <>
        "Take one down and pass it around, 1 bottle of beer on the wall.\n"
      number == 1 ->
        "1 bottle of beer on the wall, 1 bottle of beer.\n" <>
        "Take it down and pass it around, no more bottles of beer on the wall.\n"
      number == 0 ->
        "No more bottles of beer on the wall, no more bottles of beer.\n" <>
        "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
    end
  end

  @spec lyrics(Range.t()) :: String.t()
  def lyrics(range \\ 99..0) do

    Enum.map_join(range, "\n", fn num -> verse(num) end)
  end
end
